INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_SDRARRAY sdrarray)

FIND_PATH(
    SDRARRAY_INCLUDE_DIRS
    NAMES sdrarray/api.h
    HINTS $ENV{SDRARRAY_DIR}/include
        ${PC_SDRARRAY_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    SDRARRAY_LIBRARIES
    NAMES gnuradio-sdrarray
    HINTS $ENV{SDRARRAY_DIR}/lib
        ${PC_SDRARRAY_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(SDRARRAY DEFAULT_MSG SDRARRAY_LIBRARIES SDRARRAY_INCLUDE_DIRS)
MARK_AS_ADVANCED(SDRARRAY_LIBRARIES SDRARRAY_INCLUDE_DIRS)

