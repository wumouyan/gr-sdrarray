/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_SDRARRAY_DOA_ESTIMATION_H
#define INCLUDED_SDRARRAY_DOA_ESTIMATION_H

#include <sdrarray/api.h>
#include <gnuradio/block.h>

namespace gr {
  namespace sdrarray {

    /*!
     * \brief <+description of block+>
     * \ingroup sdrarray
     *
     */
    class SDRARRAY_API doa_estimation : virtual public gr::block
    {
     public:
      typedef boost::shared_ptr<doa_estimation> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of sdrarray::doa_estimation.
       *
       * To avoid accidental use of raw pointers, sdrarray::doa_estimation's
       * constructor is in a private implementation
       * class. sdrarray::doa_estimation::make is the public interface for
       * creating new instances.
       */
      static sptr make(int algorithm_type,
    		  int samples_per_snapshot,
    		  float threshold_db,
      		  float carrier_freq_hz,
              int min_Az_deg,
              int max_Az_deg,
              int min_El_deg,
              int max_El_deg);
    };

  } // namespace sdrarray
} // namespace gr

#endif /* INCLUDED_SDRARRAY_DOA_ESTIMATION_H */

