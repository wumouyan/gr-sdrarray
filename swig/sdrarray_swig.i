/* -*- c++ -*- */

#define SDRARRAY_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "sdrarray_swig_doc.i"

%{
#include "sdrarray/doa_estimation.h"
%}


%include "sdrarray/doa_estimation.h"
GR_SWIG_BLOCK_MAGIC2(sdrarray, doa_estimation);
