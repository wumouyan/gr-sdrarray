[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)


**Welcome to GR-SDRARRAY!**

This software is an out-of-tree GNURadio block library that implements basic antenna array functions in Software Defined Radio (SDR). Each block has its own GNURadio companion (GRC) wrapper and can be instantiated either in a GRC python flowgraph or directly in a C++ flowgraph.

# How to build GR-SDRARRAY

This section describes how to set up the compilation environment in GNU/Linux , and to build GR-SDRARRAY.

GNU/Linux
----------

 * Tested distributions: Ubuntu 14.04 LTS and above; Debian 8.0 "jessie" and above
 * Supported microprocessor architectures:
   * i386: Intel x86 instruction set (32-bit microprocessors).
   * amd64: also known as x86-64, the 64-bit version of the x86 instruction set, originally created by AMD and implemented by AMD, Intel, VIA and others.
   * armel: ARM embedded ABI, supported on ARM v4t and higher.
   * armhf: ARM hard float, ARMv7 + VFP3-D16 floating-point hardware extension + Thumb-2 instruction set and above.
   * arm64: ARM 64 bits or ARMv8.

Older distribution releases might work as well, but you will need GCC 4.7 or newer.

Before building GR-SDRARRAY, you need to install all the required dependencies. There are two alternatives here: through software packages or building them from the source code. It is in general not a good idea to mix both approaches.

### Alternative 1: Install dependencies using software packages

If you want to start building and running GR-SDRARRAY as quick and easy as possible, the best option is to install all the required dependencies as binary packages. 

#### Debian / Ubuntu

If you are using Debian 8, Ubuntu 14.10 or above, this can be done by copying and pasting the following line in a terminal:

~~~~~~
$ sudo apt-get install build-essential cmake git libboost-dev libboost-date-time-dev \
       libboost-system-dev libboost-filesystem-dev libboost-thread-dev libboost-chrono-dev \
       libboost-serialization-dev liblog4cpp5-dev libuhd-dev gnuradio-dev gr-osmosdr \
       libblas-dev liblapack-dev libarmadillo-dev libgflags-dev libgoogle-glog-dev python-mako  \
       python-six libmatio-dev googletest
~~~~~~

Please note that `googletest` was named `libgtest-dev` in distributions older than Debian 9 "stretch" and Ubuntu 17.04 "zesty".

**Note for Ubuntu 14.04 LTS "trusty" users:** you will need to build from source and install GNU Radio manually, as explained below, since GR-SDRARRAY requires `gnuradio-dev` >= 3.7.3, and Ubuntu 14.04 came with 3.7.2. Install all the packages above BUT EXCEPT `libuhd-dev`, `gnuradio-dev` and `gr-osmosdr` (and remove them if they are already installed in your machine), and install those dependencies using PyBOMBS. The same applies to `libmatio-dev`: Ubuntu 14.04 came with 1.5.2 and the minimum required version is 1.5.3. Please do not install the `libmatio-dev` package and install `libtool`, `automake` and `libhdf5-dev` instead. A recent version of the library will be downloaded and built automatically if CMake does not find it installed.

**Note for Debian 8 "jessie" users:** please see the note about `libmatio-dev` above. Install `libtool`, `automake` and `libhdf5-dev` instead.

Once you have installed these packages, you can jump directly to [download the source code and build GR-SDRARRAY](#download-and-build-linux).


### Alternative 2: Install dependencies using PyBOMBS

This option is adequate if you are interested in development, in working with the most recent versions of software dependencies, want more fine tuning on the installed versions, or simply in building everything from the scratch just for the fun of it. In such cases, we recommend to use [PyBOMBS](http://gnuradio.org/pybombs "Python Build Overlay Managed Bundle System wiki") (Python Build Overlay Managed Bundle System), GNU Radio's meta-package manager tool that installs software from source, or whatever the local package manager is, that automatically does all the work for you. Please take a look at the configuration options and general PyBOMBS usage at https://github.com/gnuradio/pybombs. Here we provide a quick step-by-step tutorial.

First of all, install some basic packages:

~~~~~~
$ sudo apt-get install git python-pip
~~~~~~

Download, build and install PyBOMBS:

~~~~~~
$ sudo pip install git+https://github.com/gnuradio/pybombs.git
~~~~~~

Apply a configuration:

~~~~~~
$ pybombs auto-config
~~~~~~

Add list of default recipes:

~~~~~~
$ pybombs recipes add-defaults
~~~~~~

Download, build and install GNU Radio, related drivers and some other extra modules into the directory ```/path/to/prefix``` (replace this path by your preferred one, for instance ```$HOME/sdr```):

~~~~~~
$ pybombs prefix init /path/to/prefix -a myprefix -R gnuradio-default
~~~~~~

This will perform a local installation of the dependencies under ```/path/to/prefix```, so they will not be visible when opening a new terminal. In order to make them available, you will need to set up the adequate environment variables:

~~~~~~
$ cd /path/to/prefix
$ . ./setup_env.sh
~~~~~~

Now you are ready to use GNU Radio and to jump into building GR-SDRARRAY after installing a few other dependencies.

~~~~~~
$ pybombs install armadillo gflags glog
~~~~~~


### <a name="download-and-build-linux">Clone GR-SDRARRAY's Git repository</a>:

~~~~~~
$ git clone https://bitbucket.org/jarribas/gr-sdrarray
~~~~~~

By default, you will be in the 'master' branch of the Git repository, which corresponds to the lastest stable release.


### Build and install GR-SDRARRAY

Go to GR-SDRARRAY's build directory:

~~~~~~
$ cd gr-sdrarray/build
~~~~~~

Configure and build the application:

~~~~~~
$ cmake ../
$ make
~~~~~~

By default, CMake will build the Release version, meaning that the compiler will generate a fast, optimized executable. 

Finally, use the following command to install the new GNURadio blocks into the GNURadio installation path:

~~~~~~
$ sudo make install
$ sudo ldconfig
~~~~~~

It is possible to check if everything was fine by launching gnuradio-companion

~~~~~~
$ gnuradio-companion
~~~~~~

and look for the gr-sdrarray library in the GRC block listing. It should be ready to use.


About the software license
==========================

GR-SDRARRAY is released under the [General Public License (GPL) v3](http://www.gnu.org/licenses/gpl.html), thus securing practical usability, inspection, and continuous improvement by the research community, allowing the discussion based on tangible code and the analysis of results obtained with real signals. The GPL implies that:

   1. Copies may be distributed free of charge or for money, but the source code has to be shipped or provided free of charge (or at cost price) on demand. The receiver of the source code has the same rights meaning he can share copies free of charge or resell.
   2. The licensed material may be analyzed or modified.
   3. Modified material may be distributed under the same licensing terms but *do not* have to be distributed.

That means that modifications only have to be made available to the public if distribution happens. So it is perfectly fine to take the GR-SDRARRAY source code, modify it heavily and use it in a not distributed application / library. This is how companies like Google can run their own patched versions of Linux for example.

But what this also means is that non-GPL code cannot use GPL code. This means that you cannot modify / use GR-SDRARRAY, blend it with non-GPL code, and make money with the resulting software. You cannot distribute the resulting software under a non-disclosure agreement or contract. Distributors under the GPL also grant a license for any of their patents practiced by the software, to practice those patents in GPL software. You can sell a device that runs with GR-SDRARRAY, but if you distribute the code, it has to remain under GPL.


