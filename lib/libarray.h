#include <armadillo>

arma::cx_colvec planar(arma::mat A,double Az,double El,double freq_hz);
arma::cx_mat sample_covariance(arma::cx_mat X);

arma::rowvec doa_music(arma::cx_mat Rxx,
		arma::cx_mat Gn,
		arma::mat A,
		double freq_hz,
		int min_Az_deg,
		int max_Az_deg,
		int min_El_deg,
		int max_El_deg);

arma::rowvec doa_phased(arma::cx_mat X,
		arma::mat A,
		double freq_hz,
		int min_Az_deg,
		int max_Az_deg,
		int min_El_deg,
		int max_El_deg);
